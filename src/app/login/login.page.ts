import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {
  currentPath;
  constructor(private storage: Storage) {
    this.storage.get('isFirstTimeLogin').then((value) => {
      if (value === false) {
        this.currentPath = '/sublogin';
      } else {
        this.currentPath = '/home';
      }
    });
   }
}
