import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContainerPage } from './tab-container.page';

describe('TabContainerPage', () => {
  let component: TabContainerPage;
  let fixture: ComponentFixture<TabContainerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabContainerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContainerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
