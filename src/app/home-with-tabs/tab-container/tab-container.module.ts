import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { TabContainerPage } from "./tab-container.page";

const routes: Routes = [
  {
    path: "tabs",
    component: TabContainerPage,
    children: [
      {
        path: "check-in",
        loadChildren: "../check-in/check-in.module#CheckInPageModule"
      },
      {
        path: "around-me",
        loadChildren: "../around-me/around-me.module#AroundMePageModule"
      },
      {
        path: "log-report",
        loadChildren: "../log-report/log-report.module#LogReportPageModule"
      },
      {
        path: "id-card",
        loadChildren: "../id-card/id-card.module#IdCardPageModule"
      },
      {
        path: "",
        redirectTo: "/tabs/check-in",
        pathMatch: "full"
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabContainerPage]
})
export class TabContainerPageModule {}
