import { Component, OnInit } from '@angular/core';
import { GoogleMaps, GoogleMap } from '@ionic-native/google-maps';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.page.html',
  styleUrls: ['./check-in.page.scss'],
})
export class CheckInPage implements OnInit {
  map: GoogleMap;

  constructor() { }

  async ngOnInit() {
    await this.loadMap();
  }

  loadMap() {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 3.113385,
          lng: 101.575678
        },
        zoom: 16,
        tilt: 30
      }
    });
  }

}
