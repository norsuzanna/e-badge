import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdCardPage } from './id-card.page';

describe('IdCardPage', () => {
  let component: IdCardPage;
  let fixture: ComponentFixture<IdCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
