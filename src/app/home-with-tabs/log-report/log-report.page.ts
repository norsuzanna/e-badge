import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log-report',
  templateUrl: './log-report.page.html',
  styleUrls: ['./log-report.page.scss'],
})
export class LogReportPage implements OnInit {
  public logBook = [
    {
      type: '#17B8C3',
      date: 'March 20',
      companyId: 'RA07',
      geolocation: '3.1120606, 101.4730564',
      time: '08:44 am'
    },
    {
      type: '#FF0909',
      date: 'March 20',
      companyId: 'RA07',
      geolocation: '3.1120606, 101.4730564',
      time: '11:20 am'
    }
  ];

  public userName = 'Mohd Asri Shamshudin';

  public days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

  myDate: String = new Date().toISOString();
  isFilterButtonClicked =  false;

  constructor() { }

  ngOnInit() {
  }

  selectDay(day) {
    console.log(day);
  }

  clickFilter() {
    this.isFilterButtonClicked = !this.isFilterButtonClicked;
  }

}
