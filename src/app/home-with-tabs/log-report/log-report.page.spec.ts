import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogReportPage } from './log-report.page';

describe('LogReportPage', () => {
  let component: LogReportPage;
  let fixture: ComponentFixture<LogReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogReportPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
