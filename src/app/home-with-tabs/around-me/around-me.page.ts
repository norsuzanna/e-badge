import { Component, OnInit } from '@angular/core';
import { GoogleMaps, GoogleMap } from '@ionic-native/google-maps';

@Component({
  selector: 'app-around-me',
  templateUrl: './around-me.page.html',
  styleUrls: ['./around-me.page.scss'],
})
export class AroundMePage implements OnInit {
  map: GoogleMap;

  public colleagues = [
    {
      name: 'Azah Jasman',
      image: '../assets/icon/sample-icon.png',
      company: 'Theta Edge',
      distance: '39 metre'
    },
    {
      name: 'Karen Yap',
      image: '../assets/icon/sample-icon.png',
      company: 'Theta Edge',
      distance: '39 metre'
    },
    {
      name: 'Asriza',
      image: '../assets/icon/sample-icon.png',
      company: 'Theta Edge',
      distance: '50 metre'
    },
    {
      name: 'A.Shukor',
      image: '../assets/icon/sample-icon.png',
      company: 'Theta Edge',
      distance: '70 metre'
    }
  ];

  constructor() { }

  async ngOnInit() {
    await this.loadMap();
  }

  loadMap() {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 3.113385,
          lng: 101.575678
        },
        zoom: 16,
        tilt: 30
      }
    });
  }

  searchAroundMe() {
    console.log('Search');
  }
}
