import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'sublogin.page.html',
  styleUrls: ['sublogin.page.scss'],
})

export class SubLoginPage {

  previousPath;

  constructor(private storage: Storage) {
    this.storage.get('path').then((value) => {
      this.previousPath = value;
    });

    this.storage.clear();
   }
}
