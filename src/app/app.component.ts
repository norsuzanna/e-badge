import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent {
  public appPages = [
    {
      title: 'Select Application',
      url: '/select-application',
      icon: 'home'
    },
    {
      title: 'Account',
      url: '/sublogin',
      icon: 'person'
    },
    {
      title: 'News',
      // url: '/list',
      icon: 'notifications'
    },
    {
      title: 'Setting',
      // url: '/list',
      icon: 'settings'
    },
    {
      title: 'Logout',
      url: '/login',
      icon: 'log-out',
    }
  ];

  public userName = 'Mohd Asri Shamshudin';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage
  ) {
    this.storage.set('isFirstTimeLogin', false);
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  clickAccount() {
    this.storage.set('path', 'sidemenu');
  }
}
